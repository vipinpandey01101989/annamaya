<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

?><!DOCTYPE html>

<html>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<!-- Page Description Here -->
		<!-- Disable screen scaling-->
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, user-scalable=0">	
		<!-- Place favicon.ico and apple-touch-icon(s) in the root directory -->
		<?php wp_head(); ?>
        <style>
		.popover
		{
		    width: 100%;
		    max-width: 800px;
		}
		</style>
	</head>

	<body class="hh-body alt-bg left-light">
        <!--[if lt IE 8]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

		<!-- Page Loader -->
		<div class="page-loader" id="page-loader">
			<div>
				<div class="icon ion-spin"></div>
				<p>loading</p>
			</div>
		</div> 
		<!-- BEGIN OF site header Menu -->
	    <header class="hh-header header-top">

			<div class="logo-wrapper">
			<a href="<?php echo site_url(); ?>">
				<h2 class="logo ">
					<span class="logo-images">
					<?php 
					   $custom_logo_id = get_theme_mod( 'custom_logo' );
					   if(!empty($custom_logo_id)){
					   $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
						echo "<img class='light-logo' src='".$image[0]."' alt='Logo'>";
					   }
					?>
	            </span>
					<span class="title"><?php echo get_bloginfo( 'name' ); ?></span>
				</h2>
			</a>
			</div>
			<!-- Begin of menu icon -->
			<a class="menu-icon">
				<div class="bars">
					<div class="bar1"></div>
					<div class="bar2"></div>
					<div class="bar3"></div>
				</div>
			</a>
			<!-- End of menu icon -->
			<!-- <nav class="menu-links">
				<ul class="links">
					<li class="show-for-medium-up"><a href="<?php echo site_url(); ?>/#home">Home</a></li>
					<li><a href="<?php echo site_url(); ?>/#menu">Menu</a></li>
					<li><a href="<?php echo site_url(); ?>/#about-us">About</a></li>
					<li class="show-for-medium-up"><a href="<?php echo site_url(); ?>/#location">Location</a></li>
					<li class="show-for-medium-up"><a href="" id="menu-link">More</a></li>
				</ul>
			</nav> -->
			<?php
					if ( has_nav_menu( 'primary' ) || ! has_nav_menu( 'expanded' ) ) {
						?>

							<nav class="menu-links" >

								<ul class="links">

								<?php
								if (  has_nav_menu( 'primary' ) ) {

									wp_nav_menu( array(
										'theme_location'  => 'primary',
										'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
										'container'       => 'div',
										//'container_class' => 'collapse navbar-collapse',
										'container_id'    => 'bs-example-navbar-collapse-1',
										//'menu_class'      => 'navbar-nav mr-auto',
										'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
										'walker'          => new WP_Bootstrap_Navwalker(),
									) );

								}
								?>
							
								</ul>
							</nav><!-- .primary-menu-wrapper -->

						<?php
					}

				  ?>
		</header>
		<!-- END OF site header Menu-->
		<?php $primaryNav = wp_get_nav_menu_items('primary'); ?>
		<!-- BEGIN OF menu button page navigation -->
	<nav class="page-menu">
		<div class="menu-wrapper">
			<ul id="qmenu" class="menu-list qmenu">
			<?php   foreach($primaryNav as $key=>$value)  { ?>
				<li>
					<a href="<?php echo $value->url ?>">
						<span class="title"><?php echo $value->title ?></span>
					</a>
				</li>
				<?php } ?>
			</ul>
		</div>
	</nav>
	


		<?php
		// Output the menu modal.
		//get_template_part( 'template-parts/modal-menu' );
