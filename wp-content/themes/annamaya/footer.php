<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 */

?>
    		<!-- Begin of footer contact section -->
		<div class="section section-footer section-cent" data-section="page-footer">
		
			<section class="content large clearfix">
				<!-- optional footer title -->
				<!--<h2 class="page-title">Footer</h2>-->
		
				<!-- Begin footer elements -->
				<div class="c-footer">
					<div class="wrapper row">
						<!-- Logo -->
						<div class="columns small-6 medium-4 large-2">
							<div class="f-logo">
								<img src="<?php echo get_template_directory_uri() ?>/assets/images/logo.png" alt="Logo">
								<h2>AnnaMaya</h2>
							</div>
						</div>
		
						<!-- Begin of Contact list -->
						<div class="columns small-6 medium-4 large-4">
							<!-- contact -->
							<div class="item-desc">
								<h3 class="title">Contact</h3>
								<div class="desc">
									<p>
										Call : +91 11 49031351
										<br> Email : <a href="">ouremail@domain.com</a>
									</p>
		
									<p>
										Address : Asset No.1, Aerocity, 
										<br>New Delhi – 110037,
										<br> India
									</p>
								</div>
							</div>
						</div>
						<!-- End of contact list -->
		
						<!-- Begin of Menu list -->
						<div class="columns small-6 medium-4 large-2">
							<h3>Restaurant</h3>
							<ul>
								<li><a href="<?php echo site_url(); ?>/menu-list">Breakfast</a></li>
								<li><a href="<?php echo site_url(); ?>/menu-list">Dinner</a></li>
								<li><a href="<?php echo site_url(); ?>/menu-list">Soup</a></li>
								<li><a href="<?php echo site_url(); ?>/menu-list">Coffee</a></li>
							</ul>
						</div>
						<!-- End of Menu list -->
		
						<!-- Begin of Site nav list -->
						<div class="columns small-6 medium-4 large-2">
							<h3>Navigation</h3>
							<ul>
								<li><a href="<?php echo site_url(); ?>">Home</a></li>
								<li><a href="<?php echo site_url(); ?>/about-us">About</a></li>
								<li><a href="<?php echo site_url(); ?>/contact">Contact</a></li>
								<li><a href="<?php echo site_url(); ?>/book-a-table">Book A Table</a></li>
							</ul>
						</div>
						<!-- End of  Site nav  list -->
		
						<!-- Begin of Badges/Partners list -->
						<!-- div class="columns small-6 medium-4 large-2">
							<h3>We are on</h3>
							<ul>
								<li><a>Tripadvisor</a></li>
								<li><a>Opentable</a></li>
								<li><a>Yelp</a></li>
							</ul>
						</div> -->
						<!-- End of Badges/Partners list -->
		
						<!-- Begin of registration form / social networks -->
						<div class="contact-registration contact-footer small-12 medium-12 columns">
							<div class="cf-wrapper row">
								<!-- subscription form -->
								<div class="small-12 medium-6 column reg-form-cont">
									<h3 class="title">Subscribe email to newsletter</h3>
									<form id="mail-subscription" class="form magic send_email_form row" method="get" action="ajaxserver/serverfile.php">
										<div class="columns small-12 medium-8">
											<input id="reg-email" class="email_f input" name="email" type="email" required placeholder="your@email.address" data-validation-type="email">
										</div>
										<div class="columns small-12 medium-4">
											<button id="submit-email" class="btn magic-btn email_b" name="submit_email">
		                                        <span class="txt">send</span>
		                                        <span class="arrow-icon"></span>
		                                    </button>
										</div>
										<p class="email-ok invisible"><strong>Thank you</strong> for your subscription. We will inform you.</p>
									</form>
								</div>
		
								<!-- social links -->
								<div class="social-cont small-12 medium-6 column ">
									<h3>Follow us on</h3>
									<ul class="socials">
										<li><a href="http://facebook.com/#"><i class="icon ion-social-facebook"></i></a></li>
										<li><a href="http://twitter.com/#"><i class="icon ion-social-twitter"></i></a></li>
										<li><a href="#"><i class="icon ion-social-instagram"></i></a></li>
										<li><a href="#"><i class="icon ion-social-youtube"></i></a></li>
										<li><a href="#"><i class="icon ion-social-google"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
						<!-- End of registration form -->
					</div>
				</div>
				<!-- End of footer elements -->
		
			</section>
		
		</div>
		<!-- End of footer contact section -->

	</main>
	<!-- END OF page main content -->

				<!-- BEGIN OF page footer -->
	<footer class="site-footer">
		<div class="note">
			<div class="icon"><i class="icon ion-android-map"></i></div>
			<div class="desc">
				<p>Rue Le calme Place
					<br>Au centre de Madagascar
				</p>
			</div>
		</div>
		<div class="contact">
			<ul class="socials">
				<li><a href="http://facebook.com/miradontsoa"><i class="icon ion-social-facebook"></i></a></li>
				<li><a href="http://twitter.com/miradontsoa"><i class="icon ion-social-twitter"></i></a></li>
			</ul>
		</div>
	</footer>
	<!-- END OF site footer -->	

	<!-- scripts -->
	
	<script src="<?php echo get_template_directory_uri() ?>/assets/js/vendor/jquery-1.12.4.min.js"></script>
	 <script src="<?php echo get_template_directory_uri() ?>/assets/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo get_template_directory_uri() ?>/assets/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo get_template_directory_uri() ?>/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo get_template_directory_uri() ?>/assets/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	 <script src="<?php echo get_template_directory_uri() ?>/assets/js/vendor/all.js"></script>
	<script src="<?php echo get_template_directory_uri() ?>/assets/js/particlejs/particles.min.js"></script>
	<!-- Countdown script -->
	<script src="<?php echo get_template_directory_uri() ?>/assets/js/jquery.downCount.js"></script>
	
	<!-- Form script
	<script src="<?php //echo get_template_directory_uri() ?>/assets/js/form_script.js"></script> -->
	
	<!-- Javascript main files -->
	<script src="<?php echo get_template_directory_uri() ?>/assets/js/main.js"></script>
	<!-- Google Analytics: Uncomment and change UA-XXXXX-X to be your site"s ID. -->
	<!--<script>
		(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
		function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
		e=o.createElement(i);r=o.getElementsByTagName(i)[0];
		e.src="//www.google-analytics.com/analytics.js";
		r.parentNode.insertBefore(e,r)}(window,document,"script","ga"));
		ga("create","UA-XXXXX-X");ga("send","pageview");
	</script>-->
		<script>  
		$(document).ready(function(){
         var ajax_url= '<?php echo admin_url( 'admin-ajax.php' ); ?>';
		 $('#cart-popover').popover({
			  html : true,
					container: 'body',
					content:function(){
					 return $('#popover_content_wrapper').html();
					}
			});
			
		 load_cart_data();
		function load_cart_data()
		{
		  $.ajax({
		   url:ajax_url,
		   method:"POST",
		   dataType:"json",
		   data:{action:'fetch_cart'},
		   success:function(data)
		   {
			$('#cart_details').html(data.cart_details);
			$('.total_price').text(data.total_price);
			$('.badge').text(data.total_item);
		   }
		  });
		}
		 $(document).on('click', '.add_to_cart', function(){
		  var product_id = $(this).attr("id");
		  var product_name = $('#name'+product_id+'').val();
		  var product_price = $('#price'+product_id+'').val();
		  var product_quantity = $('#quantity'+product_id).val();
		  if(product_quantity > 0)
		  {
		   $.ajax({
			url:  ajax_url,
			method:"POST",
			data:{action:'add_to_cart',product_id:product_id, product_name:product_name, product_price:product_price, product_quantity:product_quantity},
			success:function(data)
			{
			 load_cart_data();
			 alert("Item has been Added into Cart");
			}
		   });
		  }
		  else
		  {
		   alert("lease Enter Number of Quantity");
		  }
		 });
		 
		  $(document).on('click', '.delete', function(){
			  var product_id = $(this).attr("id");
			  var action = 'remove_cart';
			  if(confirm("Are you sure you want to remove this product?"))
			  {
			   $.ajax({
				url: ajax_url,
				method:"POST",
				data:{action:action,product_id:product_id},
				success:function()
				{
				 load_cart_data();
				 $('#cart-popover').popover('hide');
				 alert("Item has been removed from Cart");
				}
			   })
			  }
			  else
			  {
			   return false;
			  }
			 });

			 $(document).on('click', '#clear_cart', function(){
			  var action = 'empty_cart';
			  $.ajax({
			   url:ajax_url,
			   method:"POST",
			   data:{action:action},
			   success:function()
			   {
				load_cart_data();
				$('#cart-popover').popover('hide');
				alert("Your Cart has been clear");
			   }
			  });
			 });
		 
        });
		</script>


		<?php wp_footer(); ?>

	</body>
</html>
