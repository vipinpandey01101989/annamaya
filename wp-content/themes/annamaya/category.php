<?php
/**
* A Simple Category Template
*/
 
get_header(); 

$category = get_queried_object();
$image = get_field('image', $category);			

$args = array(
'type'                     => 'post',
'parent'                 => $category->term_id,
'orderby'                  => 'name',
'order'                    => 'ASC',
'hide_empty'               => FALSE,
'hierarchical'             => 1,
'taxonomy'                 => 'category',
); 
$child_categories = get_categories($args );

 ?> 
 
<!-- BEGIN OF page cover -->
	<div class="page-cover hh-cover">

		<!-- Transluscent mask as filter -->
		<div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="#333"></div>

	</div>
	<!--END OF page cover -->


	<!-- BEGIN OF page main content -->
	<main class="page-main hh-main page-home fullpg" id="mainpage">
        
		<!-- Begin of header cover section -->
		<div class="section section-header section-cent"  data-section="page-top-cover">
			<div class="header-cover hh-cover">
				<!-- Cover Background -->
				<?php if(!empty($image) ): ?>
				<div class="cover-bg pos-abs full-size bg-img bg-blur-0" data-image-src="<?php  echo $image['url']; ?>"></div>
				<?php endif; ?>
		
				<!-- Linear gradient mask as filter -->
				<div class="cover-bg-mask pos-abs full-size bg-gradient" data-gradient-1="#000000" data-gradient-2="rgba(0, 0, 0, 0.2)" data-opacity="0.8"></div>
				
				<!-- Transluscent mask as filter -->
				<!-- <div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="rgba(0, 0, 0, 0.7)"></div> -->
					
				<div id="main-page-bg" class="main-page-bg cover-bg-mask pos-abs full-size"></div>
			</div>
		
			
			<section class="header-text anim content large clearfix">
				<!-- centered  elements -->
				<div class="c-center">
					<div class="wrapper two-columns">
						<div class="row">
							<div class="columns small-12 medium-12">
								<!-- Header : title -->
								<header class="c-header small-text-center">
								<?php if(!empty($category->name) ): ?>
									<h2 class="title"><?php echo single_cat_title( '', false ); ?></h2>
									<?php endif; ?>
									<div class="separator"></div>
									<div class="desc">
										<p></p>
									</div>
								</header>
							</div>
		
						</div>
		
		
					</div>
				</div>
				<!-- end of centered elements -->
			</section>
		
		</div>
		<!-- End of header cover section -->
		<!-- Begin of features section -->
		<div class="section section-features section-cent fp-auto-height-responsive fh-auto section-white" data-section="features">
		
			<section class="content clearfix">
		
				<!-- Begin of centered elements -->
				<div class="c-center anim">
					<div class="wrapper">
						<!-- Header : title -->
						<header class="c-header">
							 	<div id="navbar-cart" >
								  <h2 class="title"><?php echo single_cat_title( '', false ); ?></h2>
								  <ul class="nav navbar-nav">
								   <li>
									<a id="cart-popover" class="btn" data-placement="bottom" title="Shopping Cart">
									 <i class="fa fa-shopping-cart" style="font-size:48px;color:red"></i>
									 <span class="badge"></span>
									 <span class="total_price">₹ 0.00</span>
									</a>
								   </li>
								  </ul>
								 </div>
                                <div id="popover_content_wrapper" style="display: none">
									<span id="cart_details"></span>
									<div align="right">
									 <a href="<?php echo site_url() ?>/order-page" class="btn btn-primary" id="check_out_cart">
									 <span class="glyphicon glyphicon-shopping-cart"></span> Create Order
									 </a>
									 <a href="javascript:void(0)" class="btn btn-default" id="clear_cart">
									 <span class="glyphicon glyphicon-trash"></span> Clear
									 </a>
									</div>
								 </div>
						</header>
								<div class="item-box">
									<div class="item-info">
									<?php
									// Display optional category description
									 if ( category_description() ) : ?>
									<p><?php echo category_description(); ?></p>
									<?php endif; ?>
									</div>
								</div>
					</div>
				</div>
				<!-- end of centered elements -->
			</section>
		</div>
		<!-- End of features section -->
		<!-- Begin of Products/All Menu section -->
		<div class="section section-products section-cent  bg-color" data-bgcolor="rgb(239, 239, 239)"  data-section="<?php echo $category->slug; ?>">
		    
			<section class="content large clearfix">
				<h2 class="page-title"><?php echo single_cat_title( '', false ); ?></h2>
		        <?php     
                 if(!empty($child_categories)){
				?>
		        
				<!-- centered  elements -->
				<div class="c-center anim">
					<div class="wrapper">
						<!-- begin of gallery container -->
						<div class="gallery-items">
							<!-- begin of grid gallery -->
							 <?php foreach($child_categories as $categories){  $image = get_field('image', $categories);  ?>
							     <header class="c-header">
									<h2 class="title"><?php echo $categories->name ?></h2>
								</header>
								<?php
                                   $args = array(
										'post_type' => 'product',
										'post_status' => 'publish',
										'category_name' => $categories->slug,
										'posts_per_page' => 12,
									);
									$arr_posts = new WP_Query( $args );
									 
									if ( $arr_posts->have_posts() ) : ?>
												<!-- begin of gallery container -->
												<div class="gallery-items">
													<!-- begin of grid gallery -->
													<div class="row">
													 <?php while ( $arr_posts->have_posts() ) :
													        
															$arr_posts->the_post();
															$image = get_field('product_image',$arr_posts->ID);
														 
															?> 
														<div class="col-md-4 product-grid">
														  <div class="product-image">
														    <a href="javascript:void(0)">
																<img class="pic-1" src="<?php echo $image; ?>" class="img-responsive" style="width:100%;height: 213px;" alt="Image">
																<img class="pic-2" src="<?php echo $image; ?>" class="img-responsive" style="width:100%;height: 213px;" alt="Image">
															</a>
														  </div>
														  <div class="product-content">
														    <h3 class="title"><a href="javascript:void(0)"><?php the_title(); ?></a></h3>
																<div class="price">₹<?php echo get_field('product_price',$arr_posts->ID) ?>
																</div>
																<input type="hidden" name="quantity" id="quantity<?php echo get_the_ID(); ?>" class="form-control" value="1" />
																 <input type="hidden" name="hidden_name" id="name<?php echo get_the_ID(); ?>" value="<?php the_title(); ?>" />
																 <input type="hidden" name="hidden_price" id="price<?php echo get_the_ID(); ?>" value="<?php echo get_field('product_price',get_the_ID()) ?>" />
																 <a class="add_to_cart" name="add_to_cart" id="<?php echo get_the_ID(); ?>" href="javascript:void(0)">+ Add To Cart</a>
														  </div>
														</div>
														<?php
														endwhile;
												        ?> 
													</div>
												</div>
												<!-- end of gallery container -->
									<?php endif; ?>		
							 <?php   } ?>	
						</div>
						<!-- end of gallery container -->
					</div>
				</div>
				<!-- End of centered  elements -->
				 <?php } ?>
			</section>
		</div>
		<!-- End of Products/All Menu section -->
<?php get_footer(); ?>