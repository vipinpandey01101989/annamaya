<?php
/**
 * The template for displaying the 404 template in the Twenty Twenty theme.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();  
?>

<main class="page-main hh-main page-home fullpg" id="mainpage">
	            		<!-- Begin of header cover section -->
		<div class="section section-header section-cent"  data-section="page-top-cover">
			<div class="header-cover hh-cover">
				<!-- Cover Background -->
				<?php if(!empty($image) ): ?>
				<div class="cover-bg pos-abs full-size bg-img bg-blur-0" data-image-src="<?php  echo $image['url']; ?>"></div>
				<?php endif; ?>
		
				<!-- Linear gradient mask as filter -->
				<div class="cover-bg-mask pos-abs full-size bg-gradient" data-gradient-1="#000000" data-gradient-2="rgba(0, 0, 0, 0.2)" data-opacity="0.8"></div>
				
				<!-- Transluscent mask as filter -->
				<!-- <div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="rgba(0, 0, 0, 0.7)"></div> -->
					
				<div id="main-page-bg" class="main-page-bg cover-bg-mask pos-abs full-size"></div>
			</div>
	
				<section class="header-text anim content large clearfix">
				<!-- centered  elements -->
				<div class="c-center">
					<div class="wrapper two-columns">
						<div class="row">
							<div class="columns small-12 medium-12">
								<!-- Header : title -->
								<header class="c-header small-text-center">
									<h2 class="title">
									 <?php _e( 'Page Not Found', 'twentytwenty' ); ?>
									</h2>
									<div class="separator"></div>
									<div class="desc">
										<p><?php _e( 'The page you were looking for could not be found. It might have been removed, renamed, or did not exist in the first place.', 'twentytwenty' ); ?></p>
									</div>
								</header>
							</div>
		
						</div>
		
		
					</div>
				</div>
				<!-- end of centered elements -->
			</section>
</div>


<?php
get_footer();
