<?php
/**
 * Template Name: Retail Detail Template
 *
 */

get_header();

?>
<!-- BEGIN OF page cover -->
	<div class="page-cover hh-cover">

		<!-- Transluscent mask as filter -->
		<div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="#333"></div>

	</div>
	<!--END OF page cover -->


	<!-- BEGIN OF page main content -->
	<main class="page-main hh-main page-home fullpg" id="mainpage">

		<!-- Begin of header cover section -->
		<div class="section section-header section-cent"  data-section="page-top-cover">
			<div class="header-cover hh-cover">
				<!-- Cover Background -->
				<?php if( get_field('banner_image') ): ?>
				<div class="cover-bg pos-abs full-size bg-img bg-blur-0" data-image-src="<?php the_field('banner_image'); ?>"></div>
				<?php endif; ?>
		
				<!-- Linear gradient mask as filter -->
				<div class="cover-bg-mask pos-abs full-size bg-gradient" data-gradient-1="#000000" data-gradient-2="rgba(0, 0, 0, 0.2)" data-opacity="0.8"></div>
				
				<!-- Transluscent mask as filter -->
				<!-- <div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="rgba(0, 0, 0, 0.7)"></div> -->
					
				<div id="main-page-bg" class="main-page-bg cover-bg-mask pos-abs full-size"></div>
			</div>
		
			
			<section class="header-text anim content large clearfix">
				<!-- centered  elements -->
				<div class="c-center">
					<div class="wrapper two-columns">
						<div class="row">
		
							<div class="columns small-12 medium-12">
								<!-- Header : title -->
								<header class="c-header small-text-center">
								<?php if( get_field('banner_title') ): ?>
									<h2 class="title"><?php the_field('banner_title'); ?></h2>
									<?php endif; ?>
									<div class="separator"></div>
									<div class="desc">
										<p></p>
									</div>
								</header>
							</div>
		
						</div>
		
		
					</div>
				</div>
				<!-- end of centered elements -->
			</section>
		
		</div>
		<!-- End of header cover section -->
		
		<!-- Begin of Products/All Menu section -->
		<div class="section section-products section-cent fh-auto bg-color" data-bgcolor="rgb(239, 239, 239)"  data-section="retail-list">
		
			<section class="content large clearfix">
				<h2 class="page-title">Retail List</h2>
		        <?php 
				 $category = get_category_by_slug( 'retail' );

					$args = array(
					'type'                     => 'post',
					'child_of'                 => $category->term_id,
					'orderby'                  => 'name',
					'order'                    => 'ASC',
					'hide_empty'               => FALSE,
					'hierarchical'             => 1,
					'taxonomy'                 => 'category',
					); 
					$child_categories = get_categories($args );
                    
                 if(!empty($child_categories)){
				?>
		        
				<!-- centered  elements -->
				<div class="c-center anim">
					<div class="wrapper">
						<!-- begin of gallery container -->
						<div class="gallery-items">
							<!-- begin of grid gallery -->
							<ul class="c-itemlist row small-up-1 medium-up-2">
							 <?php foreach($child_categories as $categories){  $image = get_field('image', $categories); ?>
							 
								<!-- item -->
								<li class="column anim">
									<a href="<?php echo site_url().'/detail/'.$categories->slug; ?>">
										<div class="item-img">
											<div class="img bg-img" data-image-src="<?php echo $image['url']; ?>"></div>
										</div>
										<div class="item-desc">
											<h3 class="title"><?php echo $categories->name ?></h3>
											<div class="desc">
												<p><?php echo wp_trim_words($categories->description,20,' <span style="color:rgba(9, 9, 23, 0.95)">Click here..</span>') ?></p>
											</div>
										</div>
									</a>
								</li>
							 <?php } ?>	
							</ul>
						</div>
						<!-- end of gallery container -->
					</div>
				</div>
				<!-- End of centered  elements -->
				 <?php } ?>
			</section>
		</div>
		<!-- End of Products/All Menu section -->
	<?php get_footer(); ?>
