<?php
/**
 * Template Name: Menu-list Template
 *
 */

get_header();
?>
<!-- BEGIN OF page cover -->
	<div class="page-cover hh-cover">

		<!-- Transluscent mask as filter -->
		<div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="#333"></div>

	</div>
	<!--END OF page cover -->


	<!-- BEGIN OF page main content -->
	<main class="page-main hh-main page-home fullpg" id="mainpage">

		<!-- Begin of header cover section -->
		<div class="section section-header section-cent"  data-section="page-top-cover">
			<div class="header-cover hh-cover">
				<!-- Cover Background -->
				<?php if( get_field('banner_image') ): ?>
				<div class="cover-bg pos-abs full-size bg-img bg-blur-0" data-image-src="<?php the_field('banner_image'); ?>"></div>
				<?php endif; ?>
		
		
				<!-- Linear gradient mask as filter -->
				<div class="cover-bg-mask pos-abs full-size bg-gradient" data-gradient-1="#000000" data-gradient-2="rgba(0, 0, 0, 0.2)" data-opacity="0.8"></div>
				
				<!-- Transluscent mask as filter -->
				<!-- <div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="rgba(0, 0, 0, 0.7)"></div> -->
					
				<div id="main-page-bg" class="main-page-bg cover-bg-mask pos-abs full-size"></div>
			</div>
		
			
			<section class="header-text anim content large clearfix">
				<!-- centered  elements -->
				<div class="c-center">
					<div class="wrapper two-columns">
						<div class="row">
		
							<div class="columns small-12 medium-12">
								<!-- Header : title -->
								<header class="c-header small-text-center"> <?php if( get_field('banner_title') ): ?>
									<h2><?php the_field('banner_title'); ?></h2>
								     <?php endif; ?>
									<div class="separator"></div>
									<div class="desc">
										<p></p>
									</div>
								</header>
							</div>
		
						</div>
		
		
					</div>
				</div>
				<!-- end of centered elements -->
			</section>
		
		</div>
		<!-- End of header cover section -->
	
		<!-- Begin of Products/All Menu section -->
		<div class="section section-products section-cent fh-auto bg-color" data-bgcolor="rgba(22, 16, 45, 0.95)"  data-section="menu-list">
		
			<section class="content large clearfix">
				<h2 class="page-title">Menus</h2>
		
		
				<!-- centered  elements -->
				<div class="c-center anim">
					<div class="wrapper">
						<!-- Header : title -->
						<header class="c-header">
							
							<i class="icon lnr lnr-dinner"></i>
							<p>Nos spécialités les plus rafinées. Lorem ipsum magicum dolor sit amet.</p>
						</header>
		
						<!-- begin of gallery container -->
						<div class="gallery-items">
							<!-- begin of grid gallery -->
							<ul class="c-itemlist row small-up-1 medium-up-2">
							
							      <?php  
										 $args = array(
												'type'                     => 'menu',
												'orderby'                  => 'name',
												'order'                    => 'ASC',
												'hide_empty'               => FALSE,
												'hierarchical'             => 1,
												'taxonomy'                 => 'menu_item_categories',
												); 
												$child_categories = get_categories($args );
								if(!empty($child_categories))	{ 
								  foreach($child_categories as $categories){  $image = get_field('image', $categories); 
                                    $price = get_field('price', $categories);
								?>
								<!-- item -->
								<li class="column anim">
									<a href="<?php echo get_category_link($categories); ?>">
										<div class="item-img">
											<div class="img bg-img" data-image-src="<?php echo $image; ?>"></div>
										</div>
										<div class="item-desc">
											<h3 class="title"><?php echo $categories->name ?></h3>
											<div class="desc">
												<p><?php echo wp_trim_words($categories->description,15); ?></p>
											</div>
											<h3 class="price"><?php if(!empty($price)) { echo $price; }  ?></h3>
										</div>
									</a>
								</li>
								<!-- item -->
								  <?php } 
								  } ?>		
							</ul>
						</div>
						<!-- end of gallery container -->
					</div>
				</div>
				<!-- End of centered  elements -->
		
			</section>
		</div>
		<!-- End of Products/All Menu section -->
	<?php get_footer(); ?>
