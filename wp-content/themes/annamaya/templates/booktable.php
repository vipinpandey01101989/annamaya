<?php
/**
 * Template Name: Book Table Template
 *
 */

get_header();
?>
<!-- BEGIN OF page cover -->
	<div class="page-cover hh-cover">

		<!-- Transluscent mask as filter -->
		<div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="#333"></div>

	</div>
	<!--END OF page cover -->


	<!-- BEGIN OF page main content -->
	<main class="page-main hh-main page-home fullpg" id="mainpage">

		<!-- Begin of header cover section -->
		<div class="section section-header section-cent"  data-section="page-top-cover">
			<div class="header-cover hh-cover">
				<!-- Cover Background -->
				<?php if( get_field('banner_image') ): ?>
				<div class="cover-bg pos-abs full-size bg-img bg-blur-0" data-image-src="<?php the_field('banner_image'); ?>"></div>
				<?php endif; ?>
		
		
				<!-- Linear gradient mask as filter -->
				<div class="cover-bg-mask pos-abs full-size bg-gradient" data-gradient-1="#000000" data-gradient-2="rgba(0, 0, 0, 0.2)" data-opacity="0.8"></div>
				
				<!-- Transluscent mask as filter -->
				<!-- <div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="rgba(0, 0, 0, 0.7)"></div> -->
					
				<div id="main-page-bg" class="main-page-bg cover-bg-mask pos-abs full-size"></div>
			</div>
		
			
			<section class="header-text anim content large clearfix">
				<!-- centered  elements -->
				<div class="c-center">
					<div class="wrapper two-columns">
						<div class="row">
		
							<div class="columns small-12 medium-12">
							

								<!-- Header : title -->
								<header class="c-header small-text-center">
									 <?php if( get_field('banner_title') ): ?>
									<h2><?php the_field('banner_title'); ?></h2>
								     <?php endif; ?>
									<div class="separator"></div>
									<div class="desc">
										<p></p>
									</div>
								</header>
							</div>
		
						</div>
		
		
					</div>
				</div>
				<!-- end of centered elements -->
			</section>
		
		</div>
		<!-- End of header cover section -->
		
		<!-- Begin of features section -->
		<div class="section section-features section-cent fp-auto-height-responsive fh-auto section-white" data-section="features">
		
			<section class="content clearfix">
		
				<!-- Begin of centered elements -->
				<div class="c-center anim">
					<div class="wrapper">
					    <h2>European Food Hall inspired by the colours and flavours of India</h2>
						<p>To get the most out of your visit to AnnaMaya, you may opt for online table reservation or contact us to get the best deals on food and beverages and other restaurant offers in Delhi.</p>
					</div>
				</div>
				<!-- end of centered elements -->
		
		
			</section>
		</div>
		<!-- End of features section -->
		
				<!-- Begin of book table detail section -->
		<div class="section section-location section-cent fh-auto section-white" data-section="book-table">
		
			<section class="content large clearfix">
				<h2 class="page-title">Book Table</h2>
		
				<!-- Begin of Content item -->
				<div class="content-item">
					<!-- right elements -->
					<div class="c-right anim">
						<!-- title and texts wrapper-->
						<div class="wrapper">							
							<div>
									<p><iframe src="http://reserveout.com/en/rowidget?vi=140001895&amp;style=s&amp;showPromo=true&amp;key=XLtn525PMBd30CeQ; frameborder=" width="300" height="510" scrolling="no"></iframe></p>
							</div>                            
						</div>
					</div>
					<!-- end of right elements -->
		
					<!-- left elements -->
					<div class="c-left no-clip anim">
						<div class="wrapper">
							<!-- Header : title -->
							<header class="c-header">
								<h2 class="title">Create Your Stories</h2>
							</header>
		
							<div class="c-desc">
								<ul class="iconned-list">
									<li class="item">
										<div class="desc">
											<h3 class="title">Address</h3>
											<p>Rue, Le calme place
												<br>Au centre de Paris
												<br>+00 01 23 456 789
											</p>
										</div>
									</li>
		
									<li class="item">
										<div class="icon">
											<i class="icon lnr lnr-dinner"></i>
										</div>
										<div class="desc">
											<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit.</p>
										</div>
									</li>
		
									<li class="item">
										<div class="icon">
											<i class="icon lnr lnr-coffee-cup"></i>
										</div>
										<div class="desc">
											<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit.</p>
										</div>
									</li>
								</ul>
		
								<div class="item-detail">
									<a class="normal-btn" href="#contact">
										<span class="icon"></span>
										<span class="txt">Contact us</span>
									</a>
								</div>
							</div>
		
						</div>
					</div>
					<!-- end of left elements -->
				</div>
				<!-- End of Content item -->		
			</section>
		</div>
		<!-- End of location/contact/item/product section -->
	<?php get_footer(); ?>
