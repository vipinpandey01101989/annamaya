<?php
/**
 * Template Name: Menu-item Template
 *
 */

get_header();
?>
<!-- BEGIN OF page cover -->
	<div class="page-cover hh-cover">

		<!-- Transluscent mask as filter -->
		<div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="#333"></div>

		<div id="main-page-bg" class="main-page-bg cover-bg-mask pos-abs full-size"></div>
	</div>
	<!--END OF page cover -->


	<!-- BEGIN OF page main content -->
	<main class="page-main hh-main page-home fullpg" id="mainpage">

		<!-- Begin of item detail section -->
		<div class="section section-item-detail section-menu section-cent  section-color" data-color="rgba(34, 28, 30, 0.95)" data-section="menu-item">
		
			<section class="content large clearfix">
				<!--				<h2 class="page-title">Cordon Sauté</h2>-->
		
				<!-- centered  elements -->
				<div class="c-center  anim">
					<div class="wrapper item-detail two-columns">
						<div class="row">
		
							<div class="columns small-12 medium-6">
								<!-- Header : title -->
								<header class="c-header small-text-center medium-text-left">
									<h2 class="title">Cordon Sauté</h2>
									<!--<p>LeChef secret sauce</p>-->
								</header>
								<!-- description -->
								<div class="item-desc small-text-center medium-text-left">
									<h3 class="title">Du sauté à LeChef</h3>
									<h4>Vegetables / Chicken / Caramel / Soup</h4>
									<div class="desc">
										<p>Delicacy made with passion by our LeChef, lorem ipsum magicum dolor sit amet, consectetur adipiscing elit. Maecenas
											a sem ultrices neque vehicula fermentum a sit amet nulla.</p>
									</div>
									<h3 class="price">$50</h3>
									<div class="btns">
										<a class="normal-btn" href="#menu-list">
											<span class="icon"></span>
											<span class="txt">More Soup</span>
										</a>
									</div>
								</div>
							</div>
		
							<div class="columns small-12 medium-6">
								<!-- illustration -->
								<div class="item-img">
									<!-- use image or div tag -->
									<!-- <img src="img/items/img-sample2.jpg" alt="menu image">-->
									<div class="img bg-img" data-image-src="<?php echo get_template_directory_uri() ?>/assets/images/items/img-sample2.jpg"></div>
								</div>
							</div>
		
						</div>
		
		
					</div>
				</div>
				<!-- end of centered elements -->
			</section>
		
		</div>
		<!-- End of item detail section -->
		
		<!-- Begin of Products/All Menu section -->
		<div class="section section-products section-cent fh-auto bg-color" data-bgcolor="rgba(22, 16, 45, 0.95)"  data-section="menu-list">
		
			<section class="content large clearfix">
				<h2 class="page-title">Soup</h2>
		
		
				<!-- centered  elements -->
				<div class="c-center anim">
					<div class="wrapper">
						<!-- Header : title -->
						<header class="c-header">
							<h2 class="title">Dinner Soup</h2>
							<i class="icon lnr lnr-dinner"></i>
							<h3>Tous les soirs à partir de 05h00</h3>
							<p>Nos spécialités les plus rafinées. Lorem ipsum magicum dolor sit amet.</p>
						</header>
		
						<!-- begin of gallery container -->
						<div class="gallery-items">
							<!-- begin of grid gallery -->
							<ul class="c-itemlist row small-up-1 medium-up-2">
								<!-- item -->
								<li class="column anim">
									<a href="menu-item.html">
										<div class="item-img">
											<div class="img bg-img" data-image-src="<?php echo get_template_directory_uri() ?>/assets/images/items/img-sample1.jpg"></div>
										</div>
										<div class="item-desc">
											<h3 class="title">Ramen Bleu</h3>
											<h4>Noodle / Potage</h4>
											<div class="desc">
												<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum
													a sit amet nulla.</p>
											</div>
											<h3 class="price">$50</h3>
										</div>
									</a>
								</li>
								<!-- item -->
								<li class="column anim">
									<a href="menu-item-one-col.html">
										<div class="item-img">
											<div class="img bg-img" data-image-src="<?php echo get_template_directory_uri() ?>/assets/images/items/img-sample2.jpg"></div>
										</div>
										<div class="item-desc">
											<h3 class="title">Cordon Bleu</h3>
											<h4>Fish / Potage</h4>
											<div class="desc">
												<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum
													a sit amet nulla.</p>
											</div>
											<h3 class="price">$40</h3>
										</div>
									</a>
								</li>
								<!-- item -->
								<li class="column anim">
									<a href="menu-item.html">
										<div class="item-img">
											<div class="img bg-img" data-image-src="<?php echo get_template_directory_uri() ?>/assets/images/items/img-sample3.jpg"></div>
										</div>
										<div class="item-desc">
											<h3 class="title">Cordon Soup</h3>
											<h4>Soup / Potage</h4>
											<div class="desc">
												<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum
													a sit amet nulla.</p>
											</div>
											<h3 class="price">$30</h3>
										</div>
									</a>
								</li>
								<!-- item -->
								<li class="column anim">
									<a href="menu-item-one-col.html">
										<div class="item-img">
											<div class="img bg-img" data-image-src="<?php echo get_template_directory_uri() ?>/assets/images/items/img-sample4.jpg"></div>
										</div>
										<div class="item-desc">
											<h3 class="title">Cordon Ramen</h3>
											<h4>Soup / Ramen</h4>
											<div class="desc">
												<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum
													a sit amet nulla.</p>
											</div>
											<h3 class="price">$25</h3>
										</div>
									</a>
								</li>
							</ul>
						</div>
						<!-- end of gallery container -->
					</div>
				</div>
				<!-- End of centered  elements -->
		
			</section>
		</div>
		<!-- End of Products/All Menu section -->
	<?php get_footer(); ?>
