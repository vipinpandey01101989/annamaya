<?php
/**
 * Template Name: Contact Us Template
 *
 */

get_header();
?>
<!-- BEGIN OF page main content -->
	<main class="page-main hh-main page-home fullpg" id="mainpage">

		<!-- Begin of header cover section -->
		<div class="section section-header section-cent"  data-section="page-top-cover">
			<div class="header-cover hh-cover">
				<!-- Cover Background -->
				<?php if( get_field('banner_image') ): ?>
				<div class="cover-bg pos-abs full-size bg-img bg-blur-0" data-image-src="<?php the_field('banner_image'); ?>"></div>
				<?php endif; ?>
		
				<!-- Linear gradient mask as filter -->
				<div class="cover-bg-mask pos-abs full-size bg-gradient" data-gradient-1="#000000" data-gradient-2="rgba(0, 0, 0, 0.2)" data-opacity="0.8"></div>
				
				<!-- Transluscent mask as filter -->
				<!-- <div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="rgba(0, 0, 0, 0.7)"></div> -->
					
				<div id="main-page-bg" class="main-page-bg cover-bg-mask pos-abs full-size"></div>
			</div>
		
			
			<section class="header-text anim content large clearfix">
				<!-- centered  elements -->
				<div class="c-center">
					<div class="wrapper two-columns">
						<div class="row">
		
							<div class="columns small-12 medium-12">
								<!-- Header : title -->
								<header class="c-header small-text-center">
									<?php if( get_field('banner_title') ): ?>
									<h2 class="title"><?php the_field('banner_title'); ?></h2>
									<?php endif; ?>
									<div class="separator"></div>
									<div class="desc">
										<p></p>
									</div>
								</header>
							</div>
		
						</div>
		
		
					</div>
				</div>
				<!-- end of centered elements -->
			</section>
		
		</div>
		<!-- End of header cover section -->

		
		<!-- Begin of location/contact/item/product detail section -->
		<div class="section section-location section-cent fh-auto section-white" data-section="location">
		
			<section class="content large clearfix">
				<h2 class="page-title">Location</h2>
		
				<!-- Begin of Content item -->
				<div class="content-item">
					<!-- right elements -->
					<div class="c-right anim">
						<!-- title and texts wrapper-->
						<div class="wrapper">							
							<div class="featured-map">
								<!-- add your iframe code from google maps here -->
								<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d1312.235925407806!2d2.3198181373565863!3d48.86828085543139!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2smg!4v1493927148152" width="600" height="450" allowfullscreen></iframe>
							</div>                            
						</div>
					</div>
					<!-- end of right elements -->
		
					<!-- left elements -->
					<div class="c-left no-clip anim">
						<div class="wrapper">
							<!-- Header : title -->
							<header class="c-header">
								<h2 class="title">AnnaMaya</h2>
								<h3>Open : 05h00 Am - Close : 01h30 Am</h3>
								<p>Nos spécialités les plus rafinées. Lorem ipsum magicum dolor sit amet.</p>
							</header>
		
							<div class="c-desc">
								<ul class="iconned-list">
									<li class="item">
										<div class="icon">
											<i class="icon lnr lnr-map"></i>
										</div>
										<div class="desc">
											<h3 class="title">Address</h3>
											<p>Rue, Le calme place
												<br>Au centre de Paris
												<br>+00 01 23 456 789
											</p>
										</div>
									</li>
		
									<li class="item">
										<div class="icon">
											<i class="icon lnr lnr-dinner"></i>
										</div>
										<div class="desc">
											<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit.</p>
										</div>
									</li>
		
									<li class="item">
										<div class="icon">
											<i class="icon lnr lnr-coffee-cup"></i>
										</div>
										<div class="desc">
											<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit.</p>
										</div>
									</li>
								</ul>
		
								<div class="item-detail">
									<a class="normal-btn" href="#contact">
										<span class="icon"></span>
										<span class="txt">Contact us</span>
									</a>
								</div>
							</div>
		
						</div>
					</div>
					<!-- end of left elements -->
				</div>
				<!-- End of Content item -->		
			</section>
		</div>
		<!-- End of location/contact/item/product section -->
	<?php get_footer(); ?>
