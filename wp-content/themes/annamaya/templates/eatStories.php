<?php
/**
 * Template Name: Eat Stories Template
 *
 */

get_header();
?>
<!-- BEGIN OF page cover -->
	<div class="page-cover hh-cover">

		<!-- Transluscent mask as filter -->
		<div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="#333"></div>

	</div>
	<!--END OF page cover -->


	<!-- BEGIN OF page main content -->
	<main class="page-main hh-main page-home fullpg" id="mainpage">

		<!-- Begin of header cover section -->
		<div class="section section-header section-cent"  data-section="page-top-cover">
			<div class="header-cover hh-cover">
				<!-- Cover Background -->
				<?php if( get_field('banner_image') ): ?>
				<div class="cover-bg pos-abs full-size bg-img bg-blur-0" data-image-src="<?php the_field('banner_image'); ?>"></div>
				<?php endif; ?>
		
				<!-- Linear gradient mask as filter -->
				<div class="cover-bg-mask pos-abs full-size bg-gradient" data-gradient-1="#000000" data-gradient-2="rgba(0, 0, 0, 0.2)" data-opacity="0.8"></div>
				
				<!-- Transluscent mask as filter -->
				<!-- <div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="rgba(0, 0, 0, 0.7)"></div> -->
					
				<div id="main-page-bg" class="main-page-bg cover-bg-mask pos-abs full-size"></div>
			</div>
		
			
			<section class="header-text anim content large clearfix">
				<!-- centered  elements -->
				<div class="c-center">
					<div class="wrapper two-columns">
						<div class="row">
		
							<div class="columns small-12 medium-12">
								<!-- Header : title -->
								<header class="c-header small-text-center">
									 <?php if( get_field('banner_title') ): ?>
									<h2><?php the_field('banner_title'); ?></h2>
								     <?php endif; ?>
									<div class="separator"></div>
									<div class="desc">
										<p></p>
									</div>
								</header>
							</div>
		
						</div>
		
		
					</div>
				</div>
				<!-- end of centered elements -->
			</section>
		
		</div>
		<!-- End of header cover section -->
		
		<!-- Begin of Products/All Menu section -->
		<div class="section section-products section-cent fh-auto bg-color" data-bgcolor="rgba(22, 16, 45, 0.95)"  data-section="menu-list">
		
			<section class="content large clearfix">
				<h2 class="page-title">Eat Stories</h2>
		
		
				<!-- centered  elements -->
				<div class="c-center anim">
					<div class="wrapper">
						<!-- Header : title -->
						<header class="c-header">
							<h2 class="title">Stories behind the best food in Delhi</h2>
							<i class="icon lnr lnr-dinner"></i>
							<p>The essence of AnnaMaya’s existence lies in celebrating local ingredients that are made in India – the reason behind us serving the best food in Delhi. More than that, it lies in celebrating the people who pour their heart and soul into the making of these organic products. Without the hard work of our artisans, we would not get the opportunity to utilise this delicious local produce and convert it into the best food in Delhi.</p>
							<p>Here you will find their stories, without which AnnaMaya would be incomplete.</p>
						</header>
		
						<!-- begin of gallery container -->
						<div class="gallery-items">
							<!-- begin of grid gallery -->
							<ul class="c-itemlist row small-up-1 medium-up-2">
							     <?php
                                     $args = array(
										'post_type' => 'eatstories',
										'post_status' => 'publish',
										'order' => 'DESC',
									);
									$arr_posts = new WP_Query( $args );	
									if ( $arr_posts->have_posts() ) { 
										while ( $arr_posts->have_posts() ) { 
										 $arr_posts->the_post();
			
										$content = get_the_content( 'Read more' );

										$image = get_the_post_thumbnail_url($arr_posts->ID);
										 //$image = get_field('product_image',$arr_posts->ID);
										
										?>
								<!-- item -->
								<li class="column anim">
									<a href="<?php echo get_permalink($arr_posts->ID); ?>">
										<div class="item-img">
											<div class="img bg-img" data-image-src="<?php echo $image ?>"></div>
										</div>
										<div class="item-desc">
											<h3 class="title"><?php echo get_the_title(); ?></h3>
											<div class="desc">
												<p><?php echo wp_trim_words($content,15); ?></p>
											</div>
											<h3 class="btn">Read more</h3>
										</div>
									</a>
								</li>
								
								<?php	}
									}
                                     										 
								?>	
							</ul>
						</div>
						<!-- end of gallery container -->
					</div>
				</div>
				<!-- End of centered  elements -->
		
			</section>
		</div>
		<!-- End of Products/All Menu section -->
		
	<?php get_footer(); ?>
