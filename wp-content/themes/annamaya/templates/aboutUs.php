<?php
/**
 * Template Name: About Us Template
 *
 */

get_header();
?>
<!-- BEGIN OF page cover -->
	<div class="page-cover hh-cover">

		<!-- Transluscent mask as filter -->
		<div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="#333"></div>

	</div>
	<!--END OF page cover -->


	<!-- BEGIN OF page main content -->
	<main class="page-main hh-main page-home fullpg" id="mainpage">

		<!-- Begin of header cover section -->
		<div class="section section-header section-cent"  data-section="page-top-cover">
			<div class="header-cover hh-cover">
				<!-- Cover Background -->
				<?php if( get_field('banner_image') ): ?>
				<div class="cover-bg pos-abs full-size bg-img bg-blur-0" data-image-src="<?php the_field('banner_image'); ?>"></div>
				<?php endif; ?>
		
		
				<!-- Linear gradient mask as filter -->
				<div class="cover-bg-mask pos-abs full-size bg-gradient" data-gradient-1="#000000" data-gradient-2="rgba(0, 0, 0, 0.2)" data-opacity="0.8"></div>
				
				<!-- Transluscent mask as filter -->
				<!-- <div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="rgba(0, 0, 0, 0.7)"></div> -->
					
				<div id="main-page-bg" class="main-page-bg cover-bg-mask pos-abs full-size"></div>
			</div>
		
			
			<section class="header-text anim content large clearfix">
				<!-- centered  elements -->
				<div class="c-center">
					<div class="wrapper two-columns">
						<div class="row">
		
							<div class="columns small-12 medium-12">
								<!-- Header : title -->
								<header class="c-header small-text-center">
									 <?php if( get_field('banner_title') ): ?>
									<h2><?php the_field('banner_title'); ?></h2>
								     <?php endif; ?>
									<div class="separator"></div>
									<div class="desc">
										<p></p>
									</div>
								</header>
							</div>
		
						</div>
		
		
					</div>
				</div>
				<!-- end of centered elements -->
			</section>
		
		</div>
		<!-- End of header cover section -->
		
		<!-- Begin of about us section -->
    <div class="section section-about section-cent fp-auto-height-responsive section-color" data-bgcolor="rgba(59, 44, 44, 0.95)" data-section="about-us">
		
			<section class="content clearfix">
				<h2 class="page-title">About Us</h2>
		
				<!-- Begin of centered elements -->
				<div class="c-center anim">
					<div class="wrapper">
						<!-- Header : title -->
						<header class="c-header">
		
							
							<i class="icon lnr lnr-mustache"></i>
							<p>We are special, and here is why</p>
						</header>
                        <ul class="feature-text-list row small-up-1 medium-up-2 large-up-2">
							<!-- item -->
							<li class="column anim">
								<div class="item-desc">
									<h3 class="title">Events</h3>
									<div class="desc">
										<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum
											a sit amet nulla.</p>
									</div>
								</div>
							</li>
							<!-- item -->
							<li class="column anim">
								<div class="item-desc">
									<h3 class="title">Food Delivery</h3>
									<div class="desc">
										<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum
											a sit amet nulla.</p>
									</div>
								</div>
							</li>
							<!-- item -->
							<li class="column anim">
								<div class="item-desc">
									<h3 class="title">Dinner</h3>
									<div class="desc">
										<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum
											a sit amet nulla.</p>
									</div>
								</div>
							</li>
							<!-- item -->
							<li class="column anim">
								<div class="item-desc">
									<h3 class="title">Restaurant</h3>
									<div class="desc">
										<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum
											a sit amet nulla.</p>
									</div>
								</div>
							</li>
						</ul>
						
					</div>
				</div>
				<!-- end of centered elements -->
		
		
			</section>
		
			<!-- Arrows scroll down/up -->
			<!--footer class="s-footer p-scrolldown">
				<a class="down btn" href="<?php echo site_url(); ?>/#Chef">
					<span class="left">Our</span>
					<span class="icon"></span>
					<span class="right">Menu</span>
				</a>
			</footer-->
		</div>
		<!-- End of about us section -->
	<?php get_footer(); ?>
