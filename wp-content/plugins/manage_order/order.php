<?php
/*
Plugin Name: Order
Description: Plugin to manage  Order.
*/

if ( !class_exists( 'manage_order' ) ) 
{
    class manage_order
    {
        function __construct() 
        {
	        add_action('admin_menu', array($this,'manage_order'),1); 
      
		}

		
		function manage_order()
		{
	          if(current_user_can('activate_plugins'))
            { 
            	add_menu_page('Order', esc_html( __( "Order", "manage_order" )), 'manage_options', 'manage-order' ,array( $this, 'show_Order_list' ),'',8 );
                add_submenu_page("","", esc_html( __( "", "manage_order")),1,"view-manage-order", array( $this, 'view_manage_order'));
                
				
			}
		}
		
                
		function show_order_list()
                {
                  ?> 
                      <div class="wrap">
                	
                                
                                     <div class="clear"></div>
            			<form name="view_order" id="view_order" method="POST" action="">
                         <?php
                             require("wp_order_list.php"); 
                             echo '<div class="wrap"><h2>Order List</h2> </div><br/>';?>
							
							 <?php 
                             $Order_List_Table = new Order_List_Table();
                             $Order_List_Table->views();
                             $Order_List_Table ->prepare_items();
							 $Order_List_Table->search_box('search', 'search_id');
                             $Order_List_Table ->display();
     
                           ?>      
						         <input type="hidden" name="page" value="manage-order" />	
                                </form>
                     </div>
                  <?php
                }
                
                function view_manage_order()
                {
                           ?> 
                      <div class="wrap">
                	
                                 
                                     <div class="clear"></div>
            			<form name="view-order" id="view-order" method="POST" action="">
                         <?php
                              require_once ('wp_view_manage_order_details.php');
                             
     
                           ?>      
                                </form>
                     </div>
                  <?php
                }
                
                
                
              
          //***********code for backend Plugin Menu End Here****************        
	}
         // Add Extr profile fields 
        
       


       
    $GLOBALS['manage_order'] = new manage_order();
}

?>