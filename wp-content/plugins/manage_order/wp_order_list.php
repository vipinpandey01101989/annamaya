<?php


if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
class Order_List_Table extends WP_List_Table {  

    function __construct(){
        global $status, $page;
                
        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'plugin',     //singular name of the listed records
            'plural'    => 'plugins',    //plural name of the listed records
            'ajax'      => true        //does this table support ajax?
        ) );        
    }
	

    function column_default($item, $column_name){
       switch($column_name){

            case 'col_user_id': echo $item->id;   break;
            case 'col_user_name': echo $item->userName;   break;
            case 'col_user_mail': echo $item->userEmail;   break;
			case 'col_user_mail': echo $item->userContact;   break;
			case 'col_order_amount': echo $item->total_paid_amount;   break;
	        case 'col_order_date': echo $item->created_at;   break;
			
            default:
                 print_r($item,true);//Show the whole array for troubleshooting purposes
        }
    }

/**
 * Define the columns that are going to be used in the table
 * @return array $columns, the array of columns to use with the table
 */    
function get_columns() {
   return $columns= array
   (
      'cb'=> '<input type="checkbox" />',
      'col_user_id'=>__('Order ID','manage-order'),
      'col_user_name'=>__('Customer Name','manage-order'),
      'col_user_mail'=>__('Customer Mail Address','manage-order'),
	  'col_user_contact'=>__('Customer Contact','manage-order'),
      'col_order_amount'=>__('Order Amount','manage-order'),
	  'col_order_date'=>__('Order Date','manage-order'),
        'action'=>__('Actions','manage-order')
	  
);
} 



  
   function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],  //Let's simply repurpose the table's singular label ("plugin")
            /*$2%s*/ $item->ID        //The value of the checkbox should be the record's id
        );
    }
	
	 function column_action($item){  
          $actions = array(
          	     'View' => sprintf('<a href="?page=%s&action=%s&ID=%s" >'.__("View Details","manage-order").'</a>','view-manage-order','view',$item->id),
						
        );   
            
        return sprintf('<span style="color:silver ; display : none; ">%1$s (id:%2$s)</span>%3$s',
            /*$1%s*/$item->id,
            /*$2%s*/$item->id,
            /*$3%s*/ $this->row_actions($actions)
        );
    }
	


    function prepare_items() {
	
        global $wpdb; 
        $ws_table_name = $wpdb->prefix.'orders' ;
		$order_item_table=$wpdb->prefix.'order_items' ;
        $this->manage_order($ws_table_name);	
        $per_page = 10;
        $columns = $this->get_columns();
        $hidden = array();
		$this->_column_headers = array($columns, $hidden, $sortable);       
        $this->process_bulk_action();    
		 $kv_query = "SELECT * FROM `$ws_table_name` AS ORDERS Order BY ORDERS.created_at DESC";
		 $data=$wpdb->get_results($kv_query);
         if(!empty($_REQUEST['s']))
		 {
            $str = $_REQUEST['s'];
            $kv_query = "SELECT * FROM `$ws_table_name` AS ORDERS WHERE  (ORDERS.userName LIKE '%$str%' OR ORDERS.userEmail LIKE '%$str%' OR ORDERS.userContact LIKE '%$str%') Order BY ORDERS.created_at DESC";
			 $data = $wpdb->get_results($kv_query);
		}
		
        
         $current_page = $this->get_pagenum();       
        $total_items = count($data);  
        $data = array_slice($data,(($current_page-1)*$per_page),$per_page);        
        $this->items = $data;        
        $this->set_pagination_args( array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil($total_items/$per_page)   //WE have to calculate the total number of pages
        ) );
  
		
    }
    
 
    
}



